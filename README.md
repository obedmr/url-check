url-check
=========

``url-check`` project is minimal URL or HTTP endpoint checker. It sends all possible
requests during an specified time and gather the response time. At the end a report
will be generated with basic information about the HTTP endpoint testing.

Getting the project
-------------------
```
git clone https://gitlab.com/obedmr/url-check
cd url-check/
go build
```

Using the url-check
-------------------

- **Default URL: http://gitlab.com/ Time: 300 seconds**
```
./url-check
2018/01/14 21:22:07 Request #1, Response Time: 797.376482ms
2018/01/14 21:22:08 Request #2, Response Time: 521.649985ms
2018/01/14 21:22:08 Request #3, Response Time: 542.357922ms
.
.
.
2018/01/14 21:27:06 Request #567, Response Time: 510.824001ms
2018/01/14 21:27:06 Request #568, Response Time: 513.628848ms
2018/01/14 21:27:06 URL:               https://gitlab.com
2018/01/14 21:27:06 Duration:          5m0s
2018/01/14 21:27:06 Number of request: 568
2018/01/14 21:27:06 Min request time:  488.652819ms
2018/01/14 21:27:06 Max request time:  1.671541033s
```

- **Custom URL and Time**
```
./url-check --time 5 --url https://google.com
2018/01/14 21:23:51 Request #1, Response Time: 549.210863ms
2018/01/14 21:23:51 Request #2, Response Time: 129.23662ms
2018/01/14 21:23:51 Request #3, Response Time: 135.927497ms
.
.
.
2018/01/14 21:23:56 Request #34, Response Time: 137.353815ms
2018/01/14 21:23:56 URL:               https://google.com
2018/01/14 21:23:56 Duration:          5s
2018/01/14 21:23:56 Number of request: 34
2018/01/14 21:23:56 Min request time:  126.655937ms
2018/01/14 21:23:56 Max request time:  549.210863ms
```

- ``url-check`` **help**
```
./url-check --help
Usage of ./url-check:
  -time int
        time in seconds to send HTTP requests (default 300)
  -url string
        URL to verify (default "https://gitlab.com")
```

Contact
-------
obed.n.munoz@gmail.com
