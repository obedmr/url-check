package main

import (
	"flag"
	"log"
	"time"
)

var url string
var testTime int

func init() {
	flag.StringVar(&url, "url", "https://gitlab.com", "URL to verify")
	flag.IntVar(&testTime, "time", 300,
		"time in seconds to send HTTP requests")
}

func main() {
	flag.Parse()
	duration := time.Second * time.Duration(testTime)

	err := CheckURL(url, duration)
	if err != nil {
		log.Fatal(err)
	}
}
