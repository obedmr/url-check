Contributing to ``url-check``
=============================

``url-check`` is an open source project and is open for new contributions.


Commit messages
---------------
In order to have a better clarity on project's history we encourage to follow
the following commit messages guidelines:

https://chris.beams.io/posts/git-commit/


Golang code
-----------
In order to follow Golang best practices and to have good quality code. We are
following the [Effective Go](https://chris.beams.io/posts/git-commit/) guide.


Issues
------
https://gitlab.com/obedmr/url-check/issues
