package main

import (
	"testing"
	"time"
)

func TestCheckURL(t *testing.T) {

	testCases := []struct {
		name          string
		url           string
		duration      time.Duration
		expectedError bool
	}{
		{"URL=Valid", "https://gitlab.com", time.Duration(time.Second * 1), false},
		{"URL=Invalid", "gitlab.com", time.Duration(time.Second * 1), true},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := CheckURL(tc.url, tc.duration)
			if !tc.expectedError && err != nil {
				t.Fatalf("Not expected error and got: %v", err)
			}
			if tc.expectedError && err == nil {
				t.Fatalf("Expected error and got: %v", err)
			}
		})
	}
}
