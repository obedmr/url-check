FROM golang:alpine
MAINTAINER obed.n.munoz@gmail.com

COPY ./ /go/src/gitlab.com/obedmr/url-check
RUN go get gitlab.com/obedmr/url-check

CMD /go/bin/url-check