package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

// getResponseTime performs a HTTP request to the
// specified 'url' and returns the time it took to get a
// response. In case of error, it will return the obtained
// error.
func getResponseTime(url string) (time.Duration, error) {

	start := time.Now()

	_, err := http.Get(url)
	if err != nil {
		return 0, err
	}

	t := time.Now()
	return t.Sub(start), nil
}

// CheckURL sends HTTP requests to the specified URL and
// validates its successful response during the specified duration.
// If an error occurs during any request, it will return an error,
// otherwise it will generate a full report.
func CheckURL(url string, duration time.Duration) error {

	// Channel for waiting nil or an occured error in case
	// that a request fails
	result := make(chan error)

	minTime := time.Duration(time.Second * 100)
	maxTime := time.Duration(0)
	counter := 0

	go func() {
		for {
			respTime, err := getResponseTime(url)
			if err != nil {
				result <- fmt.Errorf("Error getting access to url %v : %v",
					url, err)
				break
			}
			counter++
			log.Printf("Request #%v, Response Time: %v",
				counter, respTime)

			if maxTime < respTime {
				maxTime = respTime
			}

			if minTime > respTime {
				minTime = respTime
			}
		}
		result <- nil
	}()

	// Waits until the duration is finished
	go func() {
		time.Sleep(duration)
		result <- nil
	}()

	err := <-result
	if err != nil {
		return err
	}

	log.Printf("URL:               %v", url)
	log.Printf("Duration:          %v", duration)
	log.Printf("Number of request: %v", counter)
	log.Printf("Min request time:  %v", minTime)
	log.Printf("Max request time:  %v", maxTime)

	return nil
}
